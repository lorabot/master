"""Simple GUI for LoRaBoT control"""
import sys
import logging

from PyQt5.uic import loadUi
from PyQt5.QtCore import Qt, QThread, pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QApplication, QWidget

from core.messages import FWD, REV, LEFT, RIGHT
from core import LoraRN2483, messages


DONE = 'done'


logger = logging.getLogger(__name__)


class LoraThread(QThread):
    """Send drive commands constatnly"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cmd = None

    @pyqtSlot(str)
    def ctrl_changed(self, control_command):
        """Update command"""
        self.cmd = control_command

    def run(self):
        """Keep sending"""
        with LoraRN2483(timeout=1) as lora:
            lora.init_p2p()
            while True:
                if self.cmd == DONE:
                    self.quit()
                    return
                lora.send_command('radio tx', self.cmd, read_response=False)
                if lora.readline().strip() != b'ok':
                    logger.debug('Did not send %s', self.cmd) 
                    continue
                lora.readline()


class MainWindow(QWidget):

    ctrl_changed = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        loadUi('master/gui/g.ui', self)
        controls = (self.btn_up, self.btn_down, self.btn_left, self.btn_right)
        self.arrows = [False, False, False, False]
        self.fwd_rev_left_right = controls
        for btn in controls:
            btn.pressed.connect(self.bot_control)
            btn.released.connect(self.bot_control)
        self.speed.valueChanged.connect(self.bot_control)
        self.acceleration.valueChanged.connect(self.bot_control)
        self.lora_thread = LoraThread()
        self.ctrl_changed.connect(self.lora_thread.ctrl_changed)
        self.lora_thread.start()

    def bot_control(self):
        """Generate lora frame and feed it to transmitter"""
        buttons = tuple(x.isDown() for x in self.fwd_rev_left_right)
        dirs = [x or y for x, y in zip(buttons, self.arrows)]

        speed = self.speed.value()
        acceleration = self.acceleration.value() * 10

        exclusive = (dirs[FWD] and dirs[REV]) or (dirs[LEFT] and dirs[RIGHT])
        if not any(dirs) or exclusive:
            msg = messages.stop(acceleration)
        elif dirs[LEFT]:
            msg = messages.turn(LEFT, speed, acceleration)
        elif dirs[RIGHT]:
            msg = messages.turn(RIGHT, speed, acceleration)
        elif dirs[FWD]:
            msg = messages.drive(FWD, speed, acceleration)
        elif dirs[REV]:
            msg = messages.drive(REV, speed, acceleration)
        else:   # this shouldn't happen anyway
            msg = messges.stop(acceleration)

        self.ctrl_changed.emit(msg)

    def closeEvent(self, event):
        self.ctrl_changed.emit(DONE)

    def keyPressEvent(self, event):
        self._set_arrow(event.key(), True)

    def keyReleaseEvent(self, event):
        self._set_arrow(event.key(), False)

    def _set_arrow(self, set_key, value):
        """Update arrow's state"""
        arrows = enumerate([Qt.Key_Up, Qt.Key_Down, Qt.Key_Left, Qt.Key_Right])
        for idx, key in arrows:
            if set_key == key:
                self.arrows[idx] = value
        self.bot_control()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    #logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    wnd = MainWindow()
    wnd.show()
    sys.exit(app.exec())
